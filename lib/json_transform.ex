defmodule JSONTransform do
  @moduledoc """
  Documentation for JSONTransform.
  """

  require Logger


  def transform(template, data, opts \\ [encode: true])

  def transform(template, data, opts) when is_binary(template) do
    transform(Poison.decode!(template), data, opts)
  end

  def transform(template, data, opts) when is_binary(data) do
    transform(template, Poison.decode!(data), opts)
  end

  def transform(template, data, opts) when is_map(template) and is_map(data) do
    transformed = transform_recursive(template, data)
    if Keyword.get(opts, :encode) do
      Poison.encode!(transformed)
    else
      transformed
    end
  end

  def transform(template, data, _opts) do
    Logger.error "#{__MODULE__} invalid template or data given: #{inspect template} #{inspect data}"
    "{}"
  end


  def transform_recursive(template, data) when is_map(template) do
    template
    |> Enum.map(fn {k, v} -> {k, transform_recursive(v, data)} end)
    |> Map.new
  end

  def transform_recursive([template], data) when is_binary(template) do
    get_actual_value(template, data)
  end

  def transform_recursive([template], data) when is_map(template) do
    [transform_recursive(template, data)]
  end

  def transform_recursive([ref, template], data) do
    data_list = get_actual_value(ref, data)
    Enum.map(data_list, &transform_recursive(template, &1))
  end

  def transform_recursive("=> " <> expr, data) do
    [fun | args] = expr |> String.split(["(", ",", ")"], trim: true) |> Enum.map(&String.trim/1)
    case fun do
      "either" -> either(args, data)
      "getDate" -> get_date(args, data)
      "now" -> now(args, data)
      _ -> ""
    end
  end

  def transform_recursive(template, data) when is_binary(template) do
    get_actual_value(template, data)
  end


  @doc ~S"""

  ## Examples

      iex>JSONTransform.get_actual_value("{{foo['foo']}}", %{"foo" => %{"foo" => "bar"}})
      "bar"
      iex>JSONTransform.get_actual_value("pre {{foo}} post", %{"foo" => "bar"})
      "pre bar post"
      iex>JSONTransform.get_actual_value("static", %{"foo" => "bar"})
      "static"
      iex>JSONTransform.get_actual_value("{{not.found}}", %{"foo" => "bar"})
      ""
  """
  def get_actual_value(var, data) do
    re = ~r/\{\{\s*(.*?)\s*\}\}/
    case Regex.scan(re, var) do
      [] ->
        var

      matches ->
        matches
        |> Enum.map(fn [full_match, ref] ->
          replacement = get_in(data, ref_to_keys(ref)) || ""
          {full_match, replacement}
        end)
        |> Enum.reduce(var, fn {pattern, replacement}, acc ->
          if pattern == var, do: replacement, else: String.replace(acc, pattern, "#{replacement}")
        end)
    end
  end

  @doc ~S"""

  ## Examples

      iex>JSONTransform.ref_to_keys("foo['bar']")
      ["foo", "bar"]
      iex>JSONTransform.ref_to_keys("foo.bar")
      ["foo", "bar"]
      iex>JSONTransform.ref_to_keys("foo['bar'].foo")
      ["foo", "bar", "foo"]
  """
  def ref_to_keys(ref) do
    String.split(ref, [".", "['", "']"], trim: true)
  end


  @doc ~S"""

  ## Examples

      iex>JSONTransform.parse_args(["'foo'", "123.45", "23", "true"])
      ["foo", 123.45, 23, true]
  """
  def parse_args(args), do: args |> Enum.map(&parse_arg/1)

  def parse_arg(s) do
    case String.trim(s) do
      "'" <> s -> String.replace(s, "'", "")
      "true" -> true
      "false" -> false
      "null" -> nil
      s -> to_number(s)
    end
  end


  def to_number(s) do
    if String.contains?(s, ".") do
      case Float.parse(s) do
        {float, _} -> float
        _ -> ""
      end
    else
      case Integer.parse(s) do
        {int, _} -> int
        _ -> ""
      end
    end
  end


  def either([ref, default], data) do
    get_in(data, ref_to_keys(ref)) || parse_arg(default)
  end

  def either(_args, _data) do
    ""
  end


  def get_date([], _data) do
    DateTime.utc_now |> DateTime.to_string
  end

  def get_date([input], _data) do
    case DateTime.from_iso8601(input) do
      {:ok, dt, _} -> DateTime.to_string(dt)
      {:error, _} -> ""
    end
  end

  def get_date(_args, _data) do
    ""
  end


  def now([], _data) do
    get_date([], nil)
  end

  def now(_args, _data) do
    ""
  end
end
