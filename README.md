# JSONTransform

Transform JSON using a template

Elixir adaptation of https://github.com/robtweed/qewd-transform-json

This adaption implements all features except functions (other than the predefined `now` and `either` functions).

## Installation

Tthe package can be installed by adding `json_transform` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:json_transform, git: "git@gitlab.com:digimondo-public/json-transform.git"}
  ]
end
```

## Usage

```elixir
input_data = ~s({"foo": {"bar": "value"}})
template = %{"bar" => "{{foo.bar}}"}

JSONTransform.transform(template, input_data)
# "{\"bar\":\"value\"}"

JSONTransform.transform(template, input_data, encode: false)
# %{"bar" => "value"}
```

Both arguments (template, input data) can be either JSON encoded strings or Elixir maps with string keys.